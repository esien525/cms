<div class="box-body">
    <div class="row">
        <div class="col-md-12">
            <div class="">
                <div class="countryGroup">
                    <div class="col-md-8">
                        <p class="pull-right" style="margin-top: 10px;">
                            <a href="" class="cjsSelectAllInGroup">Select All</a> |
                            <a href="" class="cjsDeselectAllInGroup">Deselect All</a> |
                            <a href="" class="cjsSwapAllInGroup">Swap</a>
                        </p>
                    </div>
                    <div class="clearfix"></div>
                    <?php foreach (array_chunk($countries, ceil(count($countries)/2),true) as $countriesGroup): ?>
                        <div class="col-md-3">
                            <?php foreach ($countriesGroup as $countryCode => $countryName): ?>
                                <div class="checkbox">
                                    <label for="<?php echo "$countryCode" ?>">
                                        <!-- <input name="banner_country[]" type="hidden" value="false" /> -->
                                        <input id="<?php echo "$countryCode" ?>" name="banner_country[]" type="checkbox" class="flat-blue" <?php echo in_array($countryCode, $banner_countries) ? 'checked' : '' ?> value="<?php echo $countryCode;?>" /> {{ ucfirst($countryName) }}
                                    </label>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    <?php endforeach; ?>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $( document ).ready(function() {
        $('.cjsSelectAllInGroup').on('click',function (event) {
            event.preventDefault();
            $(this).closest('.countryGroup').find('input[type=checkbox]').each(function (index, value) {
                $(value).iCheck('check');
            });
        });
        $('.cjsDeselectAllInGroup').on('click',function (event) {
            event.preventDefault();
            $(this).closest('.countryGroup').find('input[type=checkbox]').each(function (index, value) {
                $(value).iCheck('uncheck');
            });
        });
        $('.cjsSwapAllInGroup').on('click',function (event) {
            event.preventDefault();
            $(this).closest('.countryGroup').find('input[type=checkbox]').each(function (index, value) {
                $(value).iCheck('toggle');
            });
        });
    });
</script>
